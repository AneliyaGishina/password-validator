# Password Validator

Create password validator with JavaScript

## Files

**HTML & JS:** https://gitlab.com/AneliyaGishina/password-validator/-/blob/main/solution.html  
**CSS:** https://gitlab.com/AneliyaGishina/password-validator/-/blob/main/validator.css

***

## Description
* Toggle checkbox for showing and hiding the password text  
* On each keystroke, validate that the string:  
    * Is between 8 and 72 characters long  
    * Contains at least 1 uppercase character  
    * Contains at least 1 lowercase character  
    * Contains at least 1 numeric character  
    * Does not match the user’s email address (e.g. the email is
bob@gmail.com and the password does not contain “bob” or
“bob@gmail.com”) which is dynamically retrieved from this endpoint:
https://run.mocky.io/v3/09e642b5-b52f-43c1-837b-8ebf70c10813  
* Adds strike out styling to the validation requirement when it is met


